# Hello, world!
#
# This is an example function named 'hello' 
# which prints 'Hello, world!'.
#

library(huxtable)

hello_statplosion <- function() {
  print("Hello, STatplosion")
}

# This parses a comma separated list of values "3,4,5,3,2" into numeric vector
parselist<-function(arg) { 
    return (as.numeric(unlist(strsplit(arg, split=","))))
}

# This creates a nice grey table display for data. Nothing fancy. 
create_hux_datatable <- function (df) {
    hux_df <- as_hux(df)
    hux_df<-theme_grey(hux_df)

    return (hux_df)
}

