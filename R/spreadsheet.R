

paste_hux_subspreadsheet <- function(m,
                            submatrix,
                            col = "A",
                            row = 1){

    I <- row
    J <- which(LETTERS == col)

    width <-ncol(submatrix) 
    height <- nrow(submatrix)

    for(j in 1:width) {
        for(i in 1:height) {
            m[I+i, J+j] = submatrix[i,j] 
        }
    }
    return (m)
}


set_hux_spreadsheet_val<- function(df, val, col=col, row=row) {
    # has header for row and col
      I <- row
      J <- which(colnames(df) == col)

    df[I+1,J] <- val 
    return (df)
} 

create_hux_spreadsheet <- function(nrows, ncols) {

    df <- matrix("",nrows ,ncols)

    rownames(df) <- 1:nrows
    colnames(df) <- LETTERS[1:ncols]

    df<-as_hux(df)
    df<-add_rownames(df, colname="")
    df<-add_colnames(df)
    df<-set_all_borders(df, brdr(0.4,"solid", "gray"))
    # df<-set_width(df,0.7)
    df<-set_wrap(df,FALSE)
    df<-set_align(df, "right")
    df<-set_position(df, "center")
    # df<-set_font_size(df,11)
    # df<-set_bold(df, row=1, col=huxtable::everywhere)
    # df<-set_bold(df, col=1, row=huxtable::everywhere)
    df<-theme_green(df)
    #df<-set_bold(df, row=huxtable::everywhere, col=huxtable::everywhere)
    df<-set_latex_float(df, "H")

    return (df)
}


toolpak_output <- function(m, 
                           model, 
                           row = 1, 
                           col="A",
                           places=6) {

    sum<-summary(model)

    multR <- sqrt(sum$r.squared)
    RSq <- sum$r.squared
    AdjR <- sum$adj.r.squared
    StdErrEstimate <- sum$sigma
    Obs <- length(sum$residuals)
    yValues <- model$model[,1]
    avgY <- mean(yValues)
    SSReg <- sum((model$fitted.values - avgY)**2)
    SSRes <- sum(model$residuals**2)
    SSTot <- SSReg+SSRes
    df1 <- sum$fstatistic["numdf"]
    df2 <- sum$fstatistic["dendf"]
    Total <- df1+df2
    MSReg <- SSReg/df1
    MSRes <- SSRes/df2
    FStat <- sum$fstatistic["value"]
    PValue <- pf(FStat,df1,df2,lower.tail=FALSE)

    # get col index from letter for column 
    colIndex <- which(LETTERS==col)

    # get 0 based index
    i <- row 
    j <- colIndex 

    m<-set_align(m, row=everywhere, col=everywhere, "right")
    m<-set_font_size(m, 11)
    m<-set_bold(m, row=everywhere,col=everywhere, value=F)

    m[i+1,j+1] <- "Summary"
    m[i+3,j+1] <- "Regression"
    m[i+4,j+1] <- "Multiple R"
    m[i+5,j+1] <- "R Square"
    m[i+6,j+1] <- "Adj R Sq"
    m[i+7,j+1] <- "Standard Err"
    m[i+8,j+1] <- "Observations"

    m<-set_align(m, row=(i+1):(i+8), col=(j+1), "left")

    m[i+4,j+2] <- round(multR,places)
    m[i+5,j+2] <- round(RSq,places)
    m[i+6,j+2] <- round(AdjR,places)
    m[i+7,j+2] <- round(StdErrEstimate,places)
    m[i+8,j+2] <- Obs

    m[i+10,j+1] <- "ANOVA"

    m<-set_align(m, row=i+10, col=j+1, "left")

    m[i+11,j+2] <- "df"
    m[i+11,j+3] <- "SS"
    m[i+11,j+4] <- "MS"
    m[i+11,j+5] <- "F"
    m[i+11,j+6] <- "Significance F"
    m<-set_align(m, row=(i+11), col=(j+2):(j+6), "center")
    m<-set_italic(m, row=(i+11), col=(j+2):(j+6))

    m[i+12,j+2] <- df1 
    m[i+13,j+2] <- df2
    m[i+14,j+2] <- Total

    m[i+12,j+3] <- round(SSReg,places)
    m[i+13,j+3] <- round(SSRes,places)
    m[i+14,j+3] <- round(SSTot,places)

    m[i+12,j+4] <- round(MSReg,places)
    m[i+13,j+4] <- round(MSRes,places)

    m[i+12,j+1] <- "Regression"
    m[i+13,j+1] <- "Residual"
    m[i+14,j+1] <- "Total"
    m<-set_align(m, row=(i+12):(i+14), col=j+1, "left")

    m[i+12,j+5] <- round(FStat,places)
    m[i+12,j+6] <- round(PValue,places)

    m[i+16,j+2] <- "Coefficients"
    m[i+16,j+3] <- "Std Err"
    m[i+16,j+4] <- "t Stat"
    m[i+16,j+5] <- "Pvalue"
    m<-set_align(m, row=(i+16), col=(j+2):(j+5), "center")
    m<-set_italic(m, row=(i+16), col=(j+2):(j+5))

    for(x in 1:length(model$coefficients)) {
        row <- i+16+x 
        col <- j+1

        m[row,col] <-  attr(model$coefficients[x],"names")
        m<-set_align(m, row=row, col=col, "left")

        m[row,j+2] <-  round(sum$coefficients[x,1],places) 
        m[row,j+3] <-  round(sum$coefficients[x,2],places) 
        m[row,j+4] <-  round(sum$coefficients[x,3],places) 
        m[row,j+5] <-  round(sum$coefficients[x,4],places) 
    }

    return (m)
}

paste_submatrix <- function(m,
                            submatrix,
                            row = 1,
                            col = "A"){

    I <- row
    J <- which(LETTERS == col)

    width <-ncol(submatrix) 
    height <- nrow(submatrix)

    for(j in 1:width) {
        for(i in 1:height) {
            m[I+(i-1), J+(j-1)] = submatrix[i,j] 
        }
    }
    return (m)
}


create_spreadsheet <- function(nrows, ncols) {

    spreadsheet <- matrix("",nrows ,ncols)

    rownames(spreadsheet) <- 1:nrows
    colnames(spreadsheet) <- LETTERS[1:ncols]

    return (spreadsheet)
}

get_subspreadsheet <- function (m, 
                                row, 
                                col, 
                                width, 
                                height) {

    columnIndex = which(LETTERS == col) 

    lastrow <- row+height-1
    lastcol <- columnIndex+width-1

    subspreadsheet <- m[row:lastrow,columnIndex:lastcol]

    rownames(subspreadsheet)<-row:lastrow
    colnames(subspreadsheet)<-LETTERS[columnIndex:lastcol]

    return (subspreadsheet)
}


create_spreadsheet_with_data<-function(columns, 
				       size=c(10,6), 
				       row=1, 
				       col="A"){
	spreadsheet <- create_blank_spreadsheet(size[1], size[2])
	spreadsheet <- insert_data_list_row_col(spreadsheet, 
                                            columns, 
                                            row,
                                            col)

	kable(spreadsheet, "markdown", row.names=TRUE)
}

insert_data<-function(spreadsheet, df, xtranames) {
    return (insert_data_row_col(spreadsheet,df,1,"A"))
}

insert_data_at<-function(spreadsheet, df, i, j) {
    return (insert_data_row_col(spreadsheet,df,i, LETTERS[j]))
}

# copy a df into the spreadsheet matrix 
insert_data_row_col<-function(spreadsheet, df, row, col) {
  return (insert_data_list_row_col(spreadsheet, 
                           as.list(df), 
                           row,      
                           col))   
}

# copy a list of columns into the spreadsheet matrix 
insert_data_list_row_col<-function(spreadsheet, #matrix of chars 
                                   columns,  # list of columns with names
                                   row,      # row number
                                   col) {    # col letter
  I <- row
  J <- which(colnames(spreadsheet) == col)

  num_cols <- length(columns)
  names <- names(columns) 

  #copy in each column vector
  for(j in 1:num_cols) {

      column<-columns[[j]]
      name<-names[j]

      #put the name of the column first
      spreadsheet[I,J+(j-1)] <- names[j]

      #Now put the data from this column after the name
      for(i in 1:length(column)) {
         spreadsheet[I+i,J+(j-1)] <- column[i]
      }
  }

  return (spreadsheet)
}



# This creates a blank spreadsheet matrix of empty char strings
create_blank_spreadsheet <- function(num_rows, num_columns) {

    m <- matrix(data=rep("",num_rows*num_columns)
                ,num_rows ,num_columns)

    colnames(m) <- LETTERS[1:num_columns]
    rownames(m) <- 1:num_rows

    return (m)
}

# This inserts m and b values or formulas 
regress_insert_m_and_b <- function(df,
                                   row,
                                   col,
                                   model,
                                   formula=TRUE) {

    m <- unname(model$coefficients[2])
    b <- unname(model$coefficients[1])

    df[row, col] <- "m" 
    df[row+1, col] <- "b" 

    if(formula == FALSE) {
        df[row, col+1] <- m 
        df[row+1, col+1] <- b
    } else {
        n <- length(model$residuals)
        xrange <- paste0("A2:A", n+1)
        yrange <- paste0("B2:B", n+1) 
        args <- paste0(yrange, ", ", xrange)
        df[row, col+1] <- sprintf("=SLOPE(%s)", args) 
        df[row+1, col+1] <- sprintf("=INTERCEPT(%s)", args) 
    }
    return (df)
} 

regression_insert_predictions <- function(df,
                                   predicters,
                                   model,
                                   formula=TRUE) {

    m <- unname(model$coefficients[2])
    b <- unname(model$coefficients[1])

    n <- length(model$residuals)

    df[1, "C"] <- "predictions" 

    df[n+2, "A"] <- predicters[1] 
    df[n+3, "A"] <- predicters[2] 

    if(formula == FALSE) {
        df[n+2, "C"] <- m*predicters[1] + b 
        df[n+3, "C"] <- m*predicters[2] + b 
    } else {
        formula1<-"=\\$F\\$2*A6+\\$F\\$3"
        df[n+2, "C"] <- formula1
        formula2<-"=\\$F\\$2*A7+\\$F\\$3"
        df[n+3, "C"] <- formula2 
    }
    return (df)
}

# This inserts predictions as values or formulas 
regress_insert_predictions <- function(df,
                                   predicters,
                                   row,
                                   col,
                                   model,
                                   formula=TRUE) {

    m <- unname(model$coefficients[2])
    b <- unname(model$coefficients[1])

    df[row, col] <- "predictions" 
    df[row+1, col] <- "year" 
    df[row+2, col] <- predicters[1] 
    df[row+3, col] <- predicters[2] 

    if(formula == FALSE) {
        df[row+2, col+1] <- m*predicters[1] + b 
        df[row+3, col+1] <- m*predicters[2] + b 
    } else {
        formula1<-"=\\$E\\$2*D7+\\$E\\$3"
        df[row+2, col+1] <- formula1
        formula2<-"=\\$E\\$2*D8+\\$E\\$3"
        df[row+3, col+1] <- formula2 
    }
    return (df)
} 

