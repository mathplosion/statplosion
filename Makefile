
all: statplosion

statplosion:
	@Rscript -e 'library(devtools)' \
			 -e 'devtools::install()'

.PHONY: all
